------------------------------------------------------
---------- PROJET DAMIEN / ALEXANDRE -----------------
------------------------------------------------------
Tips de compréhension :
- Sell_in = date de péremption de l'item
- Quality = qualité de l'item en fonction du temps

Avant toute chose, nous faisons le projet en php.

Notre première étape du projet, pour mieux le comprendre est d'introduire des tests unitaires pour vérifier chaque cas directement.
Nous prenons le jour 49 en exemple pour voir si chaque item retourne bien les bonnes valeurs.
Cela nous permet de mieux vérifier notre refacto à chaque modification du code et d'avoir dans le terminal le rapport d'erreur.\
Commit --> Feat: ajout du test unitaire nous permettant de refactorer

--------------------

Le but est d'avoir une base "Update.php" pour obtenir les différentes qualités des items.
On va avoir une fonction "appliesTo" pour savoir si ça s'applique à notre classe ou pas.
Ensuite on crée chacun des items dans un dossier Item. On teste notre premier cas avec notre classe "BrieUpdate.php".

Finalement, on va étudier tous les cas donc on implémente toutes les classes BrieUpdate, SulfuraUpdate, BackstagePasspdate et Update.
Nous décidons d'utiliser l'héritage et donc de créer la classe parente Update, et les filles BrieUpdate, SulfuraUpdate, BackstagePasspdate.
Dans GildedRose.php, nous initialisons un tableau qui va être le tableau des stratégies.
Dans ce tableau, on va instancier chaque classe que l'on a créée dans le dossier Item, dont chacune représente une stratégie pour un item donné.

On supprime tout dans la fonction UpdateQuality dans GildedRose.php car on a séparé chaque item dans des classes. Cette fonction va donc utiliser un filtre sur le tableau des stratégies pour récupérer la stratégie en fonction de l'item.\
Commit --> Feat: create an update class for all the items, then...

--------------------

On crée une nouvelle fonction dans Update.php appelée setQuality qui va ajouter la qualité correspondante à l'item.
On effectue des if qui vont vérifier si notre valeur est bien comprise entre les bornes min et max,
ou si notre valeur est supérieure à la borne max, ou si notre valeur est inférieure à la borne min.\
Commit --> Feat: fill setQuality and updateItem functions in update class

--------------------

Dans BrieUpdate, on fait la fonction UpdateItem qui va augmenter la qualité de Brie en fonction du temps passé.\
Commit --> Feat: fill updateItem function in BrieUpdate class

--------------------

On fait la même chose pour Sulfura sauf que la qualité de sulfura est toujours de 80 donc rien ne change.

--------------------

Pour BackStage Passes, on vérifie ici 4 conditions en fonction du temps. ELle augmente de 1 sa qualité comme Brie.
La qualité augmente de 2 quand il reste 10 jours ou moins et de 3 quand il reste 5 jours ou moins, mais la qualité tombe à 0 après le concert.\
Commit --> Feat: fill updateItem function in BackStagePassUpdate class

--------------------

Push pour remplacer le nom de la variable SellIn par sell_in car dans Item, la propriété s'appelle comme ça. (Je croyais que c'était du camel case)\
Commit --> FEAT: replace by because variables were not the same

--------------------
Là on définit Conjurer qui possède le même principe qu'un item basique mais la qualité est dégradée deux fois plus vite.
Création des tests pour vérifier Conjurer, pour être sûr que la qualité se dégrade deux fois plus vite que les objets normaux.\
Commit --> Feat: add ConjuredUpdate class and add test

--------------------

Ajout des tests pour les items de type BackStages Pass pour vérifier qu'apres 10 jours, la qualité diminue de 1, 
puis a partir du 10eme jour, de 2, puis du 5eme jours de 3 puis arrivé à la date de péremption de 0, ajout des tests pour AgedBrie et Sulfura.\
Commit --> Ajout des tests pour les items de type BackStages Pass...

--------------------

Ajout de l'interface UpdateInterface contenant les deux fonctions appliesTo et updateItem, pour que chaque classe implémente ces deux fonctions.\
Commit --> Ajout de l'interface...

--------------------

Nettoyage du code avec les analyseurs de code statique phpstan, phpcs et phpcbf, ce qui va nous permettre d'avoir des fonctions typées avec des paramètres typés(phpstan) et aucune erreur de standard de code avec le psr12 par exemple.\
Commit --> Feat: nettoyage du code avec phpstan, phpcs et phpcbf

--------------------

Refactoring de la fonction updateItem dans la classe BackStagePassUpdate, en créant une nouvelle fonction privée qui va renvoyer la bonne valeur.\
Commit --> Feat: refactoring updateItem function...
