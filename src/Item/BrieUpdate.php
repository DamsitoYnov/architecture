<?php

namespace GildedRose\Item;

use GildedRose\Item;

class BrieUpdate extends Update implements UpdateInterface
{
    private const AGED_BRIE = 'Aged Brie';

    public function appliesTo(Item $item): bool
    {
        return $item->name === self::AGED_BRIE;
    }

    public function updateItem(Item $item): void
    {
        $qualityIncrement = ($item->sell_in > 0) ? 1 : 2;
        parent::setQuality($item, $item->quality + $qualityIncrement);
        --$item->sell_in;
    }
}
