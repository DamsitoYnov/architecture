<?php

namespace GildedRose\Item;

use GildedRose\Item;

class SulfuraUpdate extends Update implements UpdateInterface
{
    private const SULFURAS = 'Sulfuras, Hand of Ragnaros';

    public function appliesTo(Item $item): bool
    {
        return $item->name === self::SULFURAS;
    }

    public function updateItem(Item $item): void
    {
    }
}
