<?php

namespace GildedRose\Item;

use GildedRose\Item;

class Update implements UpdateInterface
{
    public const MINIMUM_QUALITY = 0;

    public const MAXIMUM_QUALITY = 50;

    public function getMinimumQuality(): int
    {
        return self::MINIMUM_QUALITY;
    }

    public function getMaximumQuality(): int
    {
        return self::MAXIMUM_QUALITY;
    }

    public function appliesTo(Item $item): bool
    {
        return true;
    }

    public function setQuality(Item $item, int $value): void
    {
        if ($value < $this->getMaximumQuality() && $value > $this->getMinimumQuality()) {
            $item->quality = $value;
        }
        if ($value >= $this->getMaximumQuality()) {
            $item->quality = $this->getMaximumQuality();
        }
        if ($value <= $this->getMinimumQuality()) {
            $item->quality = $this->getMinimumQuality();
        }
    }

    public function updateItem(Item $item): void
    {
        $qualityDecrement = $item->sell_in < 1 ? 2 : 1;
        $this->setQuality($item, $item->quality - $qualityDecrement);
        --$item->sell_in;
    }
}
