<?php

namespace GildedRose\Item;

use GildedRose\Item;

class ConjuredUpdate extends Update implements UpdateInterface
{
    private const CONJURED = 'Conjured';

    public function appliesTo(Item $item): bool
    {
        return str_contains($item->name, self::CONJURED);
    }

    public function updateItem(Item $item): void
    {
        $qualityDecrement = $item->sell_in < 1 ? 4 : 2;
        $this->setQuality($item, $item->quality - $qualityDecrement);
        --$item->sell_in;
    }
}
