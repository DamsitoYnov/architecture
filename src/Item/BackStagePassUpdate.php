<?php

namespace GildedRose\Item;

use GildedRose\Item;

class BackStagePassUpdate extends Update implements UpdateInterface
{
    private const BACKSTAGE_PASSES = 'Backstage passes to a TAFKAL80ETC concert';

    private const INCREASE_WHEN_10_DAYS_OR_LESS = 2;

    private const INCREASE_WHEN_5_DAYS_OR_LESS = 3;

    private const INCREASE_WHEN_MORE_THAN_10_DAYS = 1;

    public function appliesTo(Item $item): bool
    {
        return $item->name === self::BACKSTAGE_PASSES;
    }

    public function updateItem(Item $item): void
    {
        $qualityIncrement = $this->setQualityIncrement($item);
        parent::setQuality($item, $item->quality + $qualityIncrement);
        --$item->sell_in;
    }

    private function setQualityIncrement(Item $item): int
    {
        if ($item->sell_in < 1) {
            return -$item->quality;
        }
        if ($item->sell_in < 6) {
            return self::INCREASE_WHEN_5_DAYS_OR_LESS;
        }
        if ($item->sell_in < 11) {
            return self::INCREASE_WHEN_10_DAYS_OR_LESS;
        }
        return self::INCREASE_WHEN_MORE_THAN_10_DAYS;
    }
}
