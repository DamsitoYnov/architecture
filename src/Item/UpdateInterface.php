<?php

namespace GildedRose\Item;

use GildedRose\Item;

interface UpdateInterface
{
    public function appliesTo(Item $item): bool;

    public function updateItem(Item $item): void;
}
