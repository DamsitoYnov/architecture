<?php

declare(strict_types=1);

namespace GildedRose;

use GildedRose\Item\BackStagePassUpdate;
use GildedRose\Item\BrieUpdate;
use GildedRose\Item\ConjuredUpdate;
use GildedRose\Item\SulfuraUpdate;
use GildedRose\Item\Update;

final class GildedRose
{
    /**
     * @var Item[]
     */
    private array $items;

    private array $updateStrategies;

    public function __construct(array $items)
    {
        $this->items = $items;
        $this->updateStrategies = [
            new BrieUpdate(),
            new ConjuredUpdate(),
            new BackStagePassUpdate(),
            new SulfuraUpdate(),
            new Update(),
        ];
    }

    public function updateQuality(): void
    {
        foreach ($this->items as $item) {
            $strategy = array_filter($this->updateStrategies, function ($strategy) use ($item) {
                return $strategy->appliesTo($item);
            });
            $strategy = current($strategy);
            $strategy->updateItem($item);
        }
    }
}
