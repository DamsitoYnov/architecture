<?php

declare(strict_types=1);

namespace Tests;

use ApprovalTests\Approvals;
use GildedRose\GildedRose;
use GildedRose\Item;
use PHPUnit\Framework\TestCase;

class GildedRoseTest extends TestCase
{
    public function testConjured(): void
    {
        $items = [
            new Item('Conjured Mana Cake', 15, 50),
        ];
        $app = new GildedRose($items);

        $days = 5;
        for ($i = 0; $i < $days; $i++) {
            $app->updateQuality();
        }
        $this->assertSame(10, $items[0]->sell_in);
        $this->assertSame(40, $items[0]->quality);
    }

    public function testConjuredWith4(): void
    {
        $items = [
            new Item('Conjured Mana Cake', 3, 50),
        ];
        $app = new GildedRose($items);

        $days = 10;
        for ($i = 0; $i < $days; $i++) {
            $app->updateQuality();
        }
        $this->assertSame(-7, $items[0]->sell_in);
        $this->assertSame(16, $items[0]->quality);
    }

    public function testDay49(): void
    {
        $items = [
            new Item('+5 Dexterity Vest', 10, 20),
            new Item('Aged Brie', 2, 0),
            new Item('Elixir of the Mongoose', 5, 7),
            new Item('Sulfuras, Hand of Ragnaros', 0, 80),
            new Item('Sulfuras, Hand of Ragnaros', -1, 80),
            new Item('Backstage passes to a TAFKAL80ETC concert', 15, 20),
            new Item('Backstage passes to a TAFKAL80ETC concert', 10, 49),
            new Item('Backstage passes to a TAFKAL80ETC concert', 5, 49),
            new Item('Conjured Mana Cake', 3, 6),
        ];

        $app = new GildedRose($items);

        $days = 49;
        for ($i = 0; $i < $days; $i++) {
            $app->updateQuality();
        }

        $this->assertSame('+5 Dexterity Vest', $items[0]->name);
        $this->assertSame(-39, $items[0]->sell_in);
        $this->assertSame(0, $items[0]->quality);

        $this->assertSame('Aged Brie', $items[1]->name);
        $this->assertSame(-47, $items[1]->sell_in);
        $this->assertSame(50, $items[1]->quality);

        $this->assertSame('Elixir of the Mongoose', $items[2]->name);
        $this->assertSame(-44, $items[2]->sell_in);
        $this->assertSame(0, $items[2]->quality);

        $this->assertSame('Sulfuras, Hand of Ragnaros', $items[3]->name);
        $this->assertSame(0, $items[3]->sell_in);
        $this->assertSame(80, $items[3]->quality);

        $this->assertSame('Sulfuras, Hand of Ragnaros', $items[4]->name);
        $this->assertSame(-1, $items[4]->sell_in);
        $this->assertSame(80, $items[4]->quality);

        $this->assertSame('Backstage passes to a TAFKAL80ETC concert', $items[5]->name);
        $this->assertSame(-34, $items[5]->sell_in);
        $this->assertSame(0, $items[5]->quality);

        $this->assertSame('Backstage passes to a TAFKAL80ETC concert', $items[6]->name);
        $this->assertSame(-39, $items[6]->sell_in);
        $this->assertSame(0, $items[6]->quality);

        $this->assertSame('Backstage passes to a TAFKAL80ETC concert', $items[7]->name);
        $this->assertSame(-44, $items[7]->sell_in);
        $this->assertSame(0, $items[7]->quality);

        $this->assertSame('Conjured Mana Cake', $items[8]->name);
        $this->assertSame(-46, $items[8]->sell_in);
        $this->assertSame(0, $items[8]->quality);
        Approvals::verifyList($items);
    }

    public function testAgeBrie(): void
    {
        $items = [
            new Item('Aged Brie', 10, 0),
        ];
        $app = new GildedRose($items);

        $days = 5;
        for ($i = 0; $i < $days; $i++) {
            $app->updateQuality();
        }
        $this->assertSame(5, $items[0]->sell_in);
        $this->assertSame(5, $items[0]->quality);
    }

    public function testAgeBrieMax(): void
    {
        $items = [
            new Item('Aged Brie', 20, 40),
        ];
        $app = new GildedRose($items);

        $days = 15;
        for ($i = 0; $i < $days; $i++) {
            $app->updateQuality();
        }
        $this->assertSame(5, $items[0]->sell_in);
        $this->assertSame(50, $items[0]->quality);
    }

    public function testAgeBrieWithSellInInf0(): void
    {
        $items = [
            new Item('Aged Brie', 5, 0),
        ];
        $app = new GildedRose($items);

        $days = 15;
        for ($i = 0; $i < $days; $i++) {
            $app->updateQuality();
        }
        $this->assertSame(-10, $items[0]->sell_in);
        $this->assertSame(25, $items[0]->quality);
    }

    public function testSulfura(): void
    {
        $items = [
            new Item('Sulfuras, Hand of Ragnaros', 0, 80),
        ];
        $app = new GildedRose($items);

        $days = 100;
        for ($i = 0; $i < $days; $i++) {
            $app->updateQuality();
        }
        $this->assertSame(0, $items[0]->sell_in);
        $this->assertSame(80, $items[0]->quality);
    }

    public function testBackStagePass(): void
    {
        $items = [
            new Item('Backstage passes to a TAFKAL80ETC concert', 12, 10),
        ];
        $app = new GildedRose($items);

        $days = 12;
        for ($i = 0; $i < $days; $i++) {
            $app->updateQuality();
            if ($i === 1) { // Premier cas : ça décrémente de 1
                $this->assertSame(10, $items[0]->sell_in);
                $this->assertSame(12, $items[0]->quality);
            }
            if ($i === 2) { // Deuxieme cas: ça décremente de 2 quand il reste 10 jours ou moins
                $this->assertSame(9, $items[0]->sell_in);
                $this->assertSame(14, $items[0]->quality);
            }
            if ($i === 7) { // Troisieme cas: ça décrementéede 3 quand il reste 5 jours ou moins
                $this->assertSame(4, $items[0]->sell_in);
                $this->assertSame(25, $items[0]->quality);
            }
            if ($i === $days - 1) {// Dernier cas: la qualité est à 0 quand sell_in est inférieure à 1
                $this->assertSame(0, $items[0]->sell_in);
                $this->assertSame(37, $items[0]->quality);
            }
        }
    }
}
